import Foundation

extension GraphComment {
  static let template = GraphComment(
    author: GraphAuthor(
      id: "VXNlci0xOTE1MDY0NDY3",
      isCreator: false,
      name: "Author McGee"
    ),
    body: "I hope you guys all remembered to write in Bat Boy/Bigfoot on your ballots! Bat Boy 2020!!",
    id: "VXNlci0yMDU3OTc4MTQ2",
    replyCount: 4
  )
}
