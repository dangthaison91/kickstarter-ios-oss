import Foundation

extension GraphCommentsEnvelope {
  static let template = GraphCommentsEnvelope(
    comments: [.template, .template, .template],
    cursor: "WzMwNDkwNDY0XQ==",
    hasNextPage: true,
    totalCount: 100
  )
}
